m = 0.0
linha = 0
fname = input("Enter file name: ")
fh = open(fname)
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue
    s = line.strip();
    l = (s[20:26])
    x = float(l)
    m = m + x
    linha = linha + 1
media = m / linha
media = round(media,12)
print("Average spam confidence:", media)
