name = input("Enter file:")

fh = open(name)
from_lines = []
emails = {}

for linha in fh:
    linha = linha.rstrip()
    if linha.find('From ') == 0:
        linha = linha.split(' ')
        email = linha[1]

        if email not in emails:
            emails[email] = 1
        else:
            emails[email] += 1
email = ''
cont = 0

for key in emails:
    if emails[key] > cont:
        cont = emails[key]
        email = key
print(email, str(cont))