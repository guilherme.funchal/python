from urllib import request
from bs4 import BeautifulSoup
import ssl

address = ' http://py4e-data.dr-chuck.net/known_by_Vinnie.html'

# Number of repeated processes
n = 0

# Position counter
x = 0


ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

while n < 7:
    html = request.urlopen(address, context=ctx).read()
    soup = BeautifulSoup(html, "html.parser")
    tags = soup('a')
    for link in tags:
        x = x + 1
        if x == 18:
            address = link.get('href', None)
            print(address)
            x = 0
            break
    n = n + 1