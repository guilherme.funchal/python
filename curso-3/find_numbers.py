import re

texto = open ('regex_sum_201161.txt')
numList = list()

for linha in texto:

    linha = linha.rstrip()
    resultados = re.findall('[0-9]+', linha)
    for respostas in resultados:
        y = int(respostas)
        numList.append(y)

print ( 'Existem ' + str(len(numList)) + ' números no arquivo e a soma total é ' + str(sum(numList)))